# Galera 3 MySQL cluster
The purpose of this repository is to run a MySQL galera cluster (3 nodes) inside Debian 9 based containers.

## Requirements

A few tools can be useful to build, run, and test this stack: 
- [docker](https://docs.docker.com/get-started/) and [docker-compose](https://docs.docker.com/compose/) to run the stack in containers;

## Run it!

### With docker compose

The compose file contained in this repository features three services:
- **node1**: the first MySQL node and cluster address reference;
- **node2**: the second MySQL node;
- **node3**: the third MySQL node;

To get the stack up and running, simply run:
`➜  ~ docker-compose up --build`

This will result into three Debian 9 running MySQL containers, communicating between themselves with the help of the WSREP replication module.

To check that clustering is operational, try running the following:
`➜  ~ docker exec -ti galera-node1 mysql -e 'show status like "wsrep_cluster_size"'`

Which should result in the following output:
```
+--------------------+-------+
| Variable_name      | Value |
+--------------------+-------+
| wsrep_cluster_size | 3     |
+--------------------+-------+
```
