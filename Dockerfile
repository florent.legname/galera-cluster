# Galera Cluster Dockerfile
FROM debian:9.13
LABEL Florent Legname florent.legname@gmail.com

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update
RUN apt-get -y install apt-transport-https ca-certificates
RUN apt-get install -y software-properties-common gnupg2
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv BC19DDBA

RUN add-apt-repository 'deb https://releases.galeracluster.com/galera-3/debian stretch main'
RUN add-apt-repository 'deb https://releases.galeracluster.com/mysql-wsrep-5.6/debian stretch main'

RUN apt-get update
RUN apt-get remove -y mysql-common
RUN apt-get install -y galera-3 galera-arbitrator-3 rsync lsof
RUN apt-get install -y mysql-common=5.6.51-25.33+1debian9
RUN apt-get install -y mysql-wsrep-common-5.6 mysql-wsrep-client-5.6 mysql-wsrep-server-5.6 mysql-wsrep-5.6

COPY ./mysql/ /etc/mysql/

# ENTRYPOINT ["mysqld"]